/********************************** (C) COPYRIGHT *******************************
* File Name          : ch32v30x_it.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2021/06/06
* Description        : Main Interrupt Service Routines.
*******************************************************************************/
#include "slider.h"
#include "ch32v30x_it.h"
#include "tos_k.h"
#include "tos_at.h"

// ��ʱ��ظ�PWM����
#define __TIM_SET_COMPARE(__HANDLE__, __CHANNEL__, __COMPARE__) \
(((__CHANNEL__) == TIM_Channel_1) ? ((__HANDLE__)->CH1CVR = (__COMPARE__)) :\
 ((__CHANNEL__) == TIM_Channel_2) ? ((__HANDLE__)->CH2CVR = (__COMPARE__)) :\
 ((__CHANNEL__) == TIM_Channel_3) ? ((__HANDLE__)->CH3CVR = (__COMPARE__)) :\
 ((__HANDLE__)->CH4CVR = (__COMPARE__)));

extern at_agent_t esp8266_tf_agent;
extern at_agent_t esp8266_agent;

void NMI_Handler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void HardFault_Handler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void USART2_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void UART6_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void UART7_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void TIM1_UP_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void TIM1_CC_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void EXTI15_10_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));

/*******************************************************************************
* Function Name  : NMI_Handler
* Description    : This function handles NMI exception.
* Input          : None
* Return         : None
*******************************************************************************/
void NMI_Handler(void)
{
}

/*******************************************************************************
* Function Name  : HardFault_Handler
* Description    : This function handles Hard Fault exception.
* Input          : None
* Return         : None
*******************************************************************************/
void HardFault_Handler(void)
{
    printf("hardfault\r\n");
    printf("mepc   = %08x\r\n",__get_MEPC());
    printf("mcause = %08x\r\n",__get_MCAUSE());
    printf("mtval  = %08x\r\n",__get_MTVAL());
    while (1)
    {

    }
}

/*********************************************************************
 * @fn      USART2_IRQHandler
 *
 * @brief   This function handles USART2 global interrupt request.
 *
 * @return  none
 */
void USART2_IRQHandler(void)
{
  uint8_t data;
  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {
      data= USART_ReceiveData(USART2);
      tos_at_uart_input_byte(&esp8266_tf_agent,data);
  }

}


/*********************************************************************
 * @fn      USART2_IRQHandler
 *
 * @brief   This function handles USART2 global interrupt request.
 *
 * @return  none
 */
void UART6_IRQHandler(void)
{
    uint8_t data;
  if(USART_GetITStatus(UART6, USART_IT_RXNE) != RESET)
  {
      data= USART_ReceiveData(UART6);
      tos_at_uart_input_byte(&esp8266_agent,data);

  }

}
/*********************************************************************
 * @fn      USART2_IRQHandler
 *
 * @brief   This function handles USART2 global interrupt request.
 *
 * @return  none
 */
void UART7_IRQHandler(void)
{
    uint8_t data;
  if(USART_GetITStatus(UART7, USART_IT_RXNE) != RESET)
  {
      data= USART_ReceiveData(UART7);
      tos_at_uart_input_byte(&esp8266_tf_agent,data);
  }

}

u32 it_cnt = 0;
void TIM1_UP_IRQHandler(void)
{
    if (it_cnt < 50)
    {
        it_cnt++;
    }
    else
    {
        GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_3) == (u8)1 ?
                            GPIO_ResetBits(GPIOE,GPIO_Pin_3) : GPIO_SetBits(GPIOE,GPIO_Pin_3);
        it_cnt = 0;
    }
    // __TIM_SET_COMPARE(TIM1, TIM_Channel_1, g_pwm_pulse);
}

void TIM1_CC_IRQHandler(void)
{
    if (it_cnt < 50)
    {
        it_cnt++;
    }
    else
    {
        GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_2) == (u8)1 ?
                            GPIO_ResetBits(GPIOE,GPIO_Pin_2) : GPIO_SetBits(GPIOE,GPIO_Pin_2);
        it_cnt = 0;
    }
}

extern u32 hall_tick;
extern u32 hall_tick_last;
extern st_slider_t g_slilder;
void EXTI15_10_IRQHandler(void)
{
    if (EXTI_GetITStatus(EXTI_Line14) != RESET)
    {
        if (hall_tick == hall_tick_last)
        {
            EXTI_ClearITPendingBit(EXTI_Line14);
            return;
        }

        g_slilder.HallCallback();
        
        hall_tick_last = hall_tick;
        EXTI_ClearITPendingBit(EXTI_Line14);
    }
}
