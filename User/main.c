/********************************** (C) COPYRIGHT *******************************
* File Name          : main.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2021/06/06
* Description        : Main program body.
*******************************************************************************/
#include "slider.h"
#include "tentacle.h"
#include "monitor.h"
#include "tos_k.h"
#include "debug.h"
#include "stdio.h"
#include "lcd_init.h"
#include "lcd.h"

#define MESSAGE_MAX 10

k_msg_q_t msg_q;
uint8_t msg_pool[MESSAGE_MAX * sizeof(void *)];


void led_key_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure = {0};

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE|RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    GPIO_SetBits(GPIOE,GPIO_Pin_2);
    GPIO_SetBits(GPIOE,GPIO_Pin_3);
    GPIO_SetBits(GPIOE,GPIO_Pin_4);
    GPIO_SetBits(GPIOE,GPIO_Pin_5);
    /* key 1 2 3 */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_8;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
}

#define MQTT_STK_SIZE 2048
k_task_t mqtt_task;
uint8_t mqtt_task_stk[MQTT_STK_SIZE];
extern void mqtt_task_entry();

#define TENTACLE_STK_SIZE 1536
k_task_t tentacle_task;
uint8_t tentacle_task_stk[TENTACLE_STK_SIZE];
extern void tentacle_task_entry(void *arg);

#define MONITOR_STK_SIZE 3096
k_task_t monitor_task;
uint8_t monitor_task_stk[MONITOR_STK_SIZE];
extern void monitor_task_entry(void* arg);

#define HALL_STK_SIZE 128
k_task_t hall_task;
uint8_t hall_task_stk[HALL_STK_SIZE];

u32 hall_tick = 0;
u32 hall_tick_last = 0;
// 用于消除抖动
void hall_task_entry(void* arg)
{
    while(1)
    {
        if (hall_tick == UINT32_MAX)
            hall_tick = 0;

        hall_tick++;
        tos_task_delay(320);
    }
}

//#define APPLICATION_TASK_STK_SIZE       8192
//k_task_t application_task;
//__aligned(4) uint8_t application_task_stk[APPLICATION_TASK_STK_SIZE];

int main(void)
{
	Delay_Init();
	USART_Printf_Init(115200);
	led_key_init();
    LCD_Init();
    LCD_Fill(0,0,LCD_W,LCD_H,BLACK);

    slider_init();
    tentable_init();    
    
    tos_knl_init();
    tos_msg_q_create(&msg_q, msg_pool, MESSAGE_MAX);
    // 去除霍尔引起的中断抖动
    tos_task_create(&hall_task, "hall", hall_task_entry, NULL, 1, hall_task_stk, HALL_STK_SIZE, 0);
    // 目前是输出转动圈数
    tos_task_create(&monitor_task, "monitor", monitor_task_entry, NULL, 2, monitor_task_stk, MONITOR_STK_SIZE, 0);
    // 防抖按钮
//    tos_task_create(&tentacle_task, "tentacle", tentacle_task_entry, NULL, 3, tentacle_task_stk, TENTACLE_STK_SIZE, 0); // Create tentacle_task
    tos_task_create(&mqtt_task, "mqtt", mqtt_task_entry, NULL, 3, mqtt_task_stk, MQTT_STK_SIZE, 0); // Create tentacle_task
    //tos_task_create(&application_task, "application_task", application_entry, NULL, 4, application_task_stk, APPLICATION_TASK_STK_SIZE, 0);
    tos_knl_start();
    printf("should not run at here!\r\n");
}
