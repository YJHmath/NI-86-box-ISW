/*
 * monitor.c
 *
 *  Created on: Jul 17, 2022
 *      Author: Administrator
 */
#include "monitor.h"
#include "slider.h"
#include "tentacle.h"
#include "tos_k.h"
#include "lcd.h"

uint32_t g_sw_status = 0;
uint32_t g_sw_status_pst = 0;

extern st_slider_t g_slilder;
extern uint32_t cur_round_;

uint8_t get_key(void)
{
    uint16_t value=0;
    if((GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0) & GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)\
         & GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_8)) == 0 )
    {
        //tos_task_delay(15);
        // Delay_Ms(15);
        if((GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0) & GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)\
             & GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_8)) == 0 )
        {
           value= GPIO_ReadInputData(GPIOA);
           if((value&0x1)==0) return 1;
           else if((value&0x2)==0)return 2;
           else if((value&0x100)==0) return 3;
           else return 0;
        }
    }
   return 0;
}

void monitor_task_entry(void* arg)
{
    int ret = 0;
    const static char strA[] = "sw    :";
    const static char strB[] = "sw_pst:";
    char outputStr[2][64];
    char outputBits[32] = {0};

    while (1)
    {
//        printf("prepare get_key()\n");
        switch (get_key())
        {
            case 1:
                g_sw_status = (g_sw_status & 0x01) ? g_sw_status & ~0x01 : g_sw_status | 0x01;
                g_sw_status = (g_sw_status & 0x02) ? g_sw_status & ~0x02 : g_sw_status | 0x02;
                g_sw_status = (g_sw_status & 0x04) ? g_sw_status & ~0x04 : g_sw_status | 0x04;
                break;
            case 2:
                moveBackward();
                // g_sw_status = (g_sw_status & 0x02) ? g_sw_status & ~0x02 : g_sw_status | 0x02;
                tentable_push(PUSH_METHOD_ON);
                break;
            case 3:
                stopMoving();
                // g_sw_status = (g_sw_status & 0x04) ? g_sw_status & ~0x04 : g_sw_status | 0x04;
                tentable_push(PUSH_METHOD_OFF);
                break;
            default:
                break;
        }
        memcpy(outputStr[0], strA, sizeof(strA));
        trans_int_to_16byteChArr(g_sw_status, outputBits);
        strcat(outputStr[0], outputBits);
        LCD_ShowString(0, 0, outputStr[0], WHITE, BLACK, 16, 0);

        memcpy(outputStr[1], strB, sizeof(strB));
        trans_int_to_16byteChArr(g_sw_status_pst, outputBits);
        strcat(outputStr[1], outputBits);
        LCD_ShowString(0, 16, outputStr[1], WHITE, BLACK, 16, 0);

//        printf("cur_round_:%d\n", cur_round_);
        // LCD_ShowString(0, 32, outputStr[0], WHITE, BLACK, 16, 0);
        while(g_sw_status != g_sw_status_pst)
        {
            ret = slider_moveNextStep();
            if (ret < 0)
            {
                if (ret == -2) // setDirection(SLIDER_DIR_BACKWARD);
                {
                    printf("由 反向 -> 正向\n");
                    continue;
                }
                else if(ret == -3) // setDirection(SLIDER_DIR_FORWARD);
                {
                    printf("由 正向 -> 反向\n");
                    continue;
                }
                else
                {
                    printf("moveNextStep=>%d\n", ret);
                    break;
                }
            }
            on_over_key_pos(ret);
        }
        if (g_sw_status == g_sw_status_pst)
        {
            slider_moveOrigin();
        }
        tos_task_delay(100);
    }
}

void on_over_key_pos(int cur_key_pos)
{
    printf("当前处于%d号按键上\n", cur_key_pos);
    int tmp = 1 << (cur_key_pos - 1);
    if (((g_sw_status ^ g_sw_status_pst) & tmp) != 0)
    {
        if ((g_sw_status & tmp) != 0 && (g_sw_status_pst & tmp) == 0)
        {
            // 按键从关闭-->打开
            printf("打开按键\n");
            tentable_push(PUSH_METHOD_ON);
            g_sw_status_pst = g_sw_status_pst | (g_sw_status & tmp);
        }
        else //if (g_sw_status & tmp == 0 && g_sw_status_pst & tmp != 0)
        {
            // 按键从打开-->关闭
            printf("关闭按键\n");
            tentable_push(PUSH_METHOD_OFF);
            g_sw_status_pst = g_sw_status_pst & ~(g_sw_status_pst & tmp);
        }
        tos_task_delay(1000);
    }
}

void trans_int_to_16byteChArr(uint32_t val, char* outputArr)
{
    int i = 0, tmp = 0;
//    for (i = 0; i < 16;i++)
    for (i = 0; i < 4;i++)
    {
        tmp = 1 << i;
        outputArr[i] = (val & tmp) ? '1' : '0';
    }
}
