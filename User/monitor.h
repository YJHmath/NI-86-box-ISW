/*
 * monitor.h
 *
 *  Created on: Jul 17, 2022
 *      Author: Administrator
 */

#ifndef USER_MONITOR_H_
#define USER_MONITOR_H_
#include "stdint.h"
//void monitor_task_entry(void* arg);
uint8_t get_key(void);
void trans_int_to_16byteChArr(uint32_t val, char* outputArr);
void on_over_key_pos(int cur_key_pos);

#endif /* USER_MONITOR_H_ */
