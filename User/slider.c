/*
 * slider.c
 *
 *  Created on: Jul 15, 2022
 *      Author: MrYe1
 */
#include "slider.h"
#include "tos_k.h"
#include "ch32v30x.h"

st_slider_t g_slilder;
int key_pos_round_[16];
uint32_t cur_round_ = 0;
uint32_t min_round_ = 0;
uint32_t max_round_ = 0;
en_slider_dir_t dir_ = SLIDER_DIR_FORWARD;
st_slider_t* s = 0; // 代替this

extern uint32_t g_sw_status;
extern uint32_t g_sw_status_pst;

// PC3/TIM10_CH3    ---电机PWM, 无需映射
// PC2              ---电机DIR
// PB15             ---电机使能
// PB14             ---霍尔D0
void slider_hardware_init()
{
    // 输出固定的PWM频率
    u16 arr = 100;
    u16 psc = 144 - 1;
    u16 ccp = 50;

    GPIO_InitTypeDef GPIO_InitStructure = {0};
    EXTI_InitTypeDef EXTI_InitStructure = {0};
    NVIC_InitTypeDef NVIC_InitStructure = {0};
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure = {0};
    TIM_OCInitTypeDef TIM_OCInitStructure = {0};

    RCC_APB2PeriphClockCmd(
        RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOB | RCC_APB2Periph_TIM10 | RCC_APB2Periph_AFIO,
        ENABLE);
    // PB14             ---霍尔D0
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    // PB14 ---> EXIT_Line0
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource14);
    EXTI_InitStructure.EXTI_Line = EXTI_Line14;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    // PC3/TIM10_CH3    ---电机PWM, 无需映射
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    TIM_TimeBaseInitStructure.TIM_Period = arr;
    TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM10, &TIM_TimeBaseInitStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = ccp;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC3Init(TIM10, &TIM_OCInitStructure);
    TIM_CtrlPWMOutputs(TIM10, ENABLE);
    TIM_OC1PreloadConfig(TIM10, TIM_OCPreload_Disable);
    TIM_ARRPreloadConfig(TIM10, ENABLE);
    TIM_Cmd(TIM10, ENABLE);
    TIM_CCxCmd(TIM10, TIM_Channel_3, TIM_CCx_Enable);
    
    // PC2              ---电机DIR
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    // PB15             ---电机使能, 低电平停转
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_ResetBits(GPIOC, GPIO_Pin_2);      // 定义:从左到右(远离电机方向), 正方向
    GPIO_ResetBits(GPIOB, GPIO_Pin_15);     // 让电机停转
}

void slider_init()
{
    slider_hardware_init();

    s = &g_slilder;
    for (int i = 0 ; i < 16; i++)
        key_pos_round_[i] = -1;

    key_pos_round_[0] = 14; // 起始位置
    key_pos_round_[1] = 25;
    key_pos_round_[2] = 36;
    key_pos_round_[3] = 46;
    // 初始化时默认触手处于起始位置
    min_round_ = key_pos_round_[0];
    max_round_ = key_pos_round_[3];
    cur_round_ = key_pos_round_[0];

    s->max_key_N_ = 3;
    s->HallCallback = onHallCallback;
}

int slider_moveOrigin()
{
    int ret = 0;
    if (s->cur_key_N_ != 0)
    {
        printf("prepare slider_moveOrigin\n");
        moveBackward();
        while (cur_round_ > min_round_)
        {
            if (g_sw_status != g_sw_status_pst)
                break;

            tos_task_delay(100);
        }
        s->cur_key_N_ = 0;
        stopMoving();
    }
    return ret;
}

void onHallCallback()
{
    if (dir_ == SLIDER_DIR_FORWARD)
    {
        GPIO_ResetBits(GPIOE,GPIO_Pin_2);
        cur_round_++;
    }
    else if (dir_ == SLIDER_DIR_BACKWARD)
    {
        GPIO_ResetBits(GPIOE,GPIO_Pin_3);
        cur_round_--;
    }
}

int slider_moveNextStep()
{
    int ret = -1;
    printf("current dir_:%d\n", dir_);
    if (dir_ == SLIDER_DIR_FORWARD)
    {
        if (cur_round_ >= max_round_)
        {
            printf("s->cur_key_N_:%d, cur_round_:%d\n", s->cur_key_N_, cur_round_);
            setDirection(SLIDER_DIR_BACKWARD);
            return -2;
        }
        moveForward();
        printf("[forward]cur_round_:%d\n", cur_round_);
        while((ret = getKeyPos()) == -1)
        {
            printf("cur_round_:%d\n", cur_round_);
            tos_task_delay(100);
        }
        printf("cur_round_:%d\n", cur_round_);
        stopMoving();
    }
    else if (dir_ == SLIDER_DIR_BACKWARD)
    {
        if (cur_round_ <= min_round_)
        {
            printf("s->cur_key_N_:%d, cur_round_:%d\n", s->cur_key_N_, cur_round_);
            setDirection(SLIDER_DIR_FORWARD);
            return -3;
        }
        moveBackward();
        printf("[backward]cur_round_:%d\n", cur_round_);
        while((ret = getKeyPos()) == -1)
        {
            printf("cur_round_:%d\n", cur_round_);
            tos_task_delay(100);
        }
        printf("cur_round_:%d\n", cur_round_);
        stopMoving();
    }
    printf("after nextStep\n");
    s->cur_key_N_ = ret;
    return ret;
}

void setDirection(en_slider_dir_t dir)
{
    switch(dir)
    {
    case SLIDER_DIR_FORWARD:
        GPIO_ResetBits(GPIOC, GPIO_Pin_2);
        dir_ = SLIDER_DIR_FORWARD;
        break;
    case SLIDER_DIR_BACKWARD:
        GPIO_SetBits(GPIOC, GPIO_Pin_2);
        dir_ = SLIDER_DIR_BACKWARD;
        break;
    default:
        break;
    }
}

void moveForward()
{
    setDirection(SLIDER_DIR_FORWARD);
    GPIO_SetBits(GPIOB, GPIO_Pin_15);
}

void moveBackward()
{
    setDirection(SLIDER_DIR_BACKWARD);
    GPIO_SetBits(GPIOB, GPIO_Pin_15);
}

void stopMoving()
{
    GPIO_ResetBits(GPIOB, GPIO_Pin_15);
}

int getKeyPos()
{
    int ret = -1;
    int span = 0;
    int max = 0 , min = 0;
    int new_key_N = 0;
    if (dir_ == SLIDER_DIR_FORWARD)
    {
        new_key_N = s->cur_key_N_ + 1;
        if (cur_round_ >= key_pos_round_[new_key_N])
        {
            ret = new_key_N;
        }
    }
    else //  SLIDER_DIR_BACKWARD
    {
        new_key_N = s->cur_key_N_ - 1;
        if (cur_round_ <= key_pos_round_[new_key_N])
        {
            ret = new_key_N;
        }
    }
    return ret;
}
