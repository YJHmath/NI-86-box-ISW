/*
 * slider.h
 *
 *  Created on: Jul 15, 2022
 *      Author: MrYe1
 */

#ifndef USER_SLIDER_H_
#define USER_SLIDER_H_
#include <stdint.h>

typedef enum en_slider_dir
{
    SLIDER_DIR_FORWARD,
    SLIDER_DIR_BACKWARD
} en_slider_dir_t;

typedef void (*hall_callback_fn)();

typedef struct st_slider
{
// public
    int32_t cur_key_N_;
    int32_t max_key_N_;
    hall_callback_fn HallCallback;
} st_slider_t;

// public
void slider_init();
int slider_moveNextStep();
int slider_moveOrigin();

void onHallCallback();
void setDirection(en_slider_dir_t dir);

// private
void moveForward();
void moveBackward();
void stopMoving();

/*************************************************
 Description:
 Input:
 Output:
 Return: -1.不在任何一个键位上; 0.在起始键位上; 1.在1号键位上; ...
 Others:
*************************************************/
int getKeyPos();

#endif /* USER_SLIDER_H_ */
