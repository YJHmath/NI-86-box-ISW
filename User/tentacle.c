/*
 * tentacle.c
 *
 *  Created on: Jul 17, 2022
 *      Author: Administrator
 */
#include "tentacle.h"
#include "tos_k.h"
#include "ch32v30x.h"

#define PWM_HL_OFF      2200
#define PWM_HL_NORMAL 1500
#define PWM_HL_ON       1000
#define STK_SIZE_TASK_SW            512
#define STK_SIZE_TASK_SENDER        512
#define PRIO_TASK_SW_PRIO           4
#define MESSAGE_MAX                 10
// 随时随地改PWM脉宽
#define __TIM_SET_COMPARE(__HANDLE__, __CHANNEL__, __COMPARE__)\
(((__CHANNEL__) == TIM_Channel_1) ? ((__HANDLE__)->CH1CVR = (__COMPARE__)) :\
 ((__CHANNEL__) == TIM_Channel_2) ? ((__HANDLE__)->CH2CVR = (__COMPARE__)) :\
 ((__CHANNEL__) == TIM_Channel_3) ? ((__HANDLE__)->CH3CVR = (__COMPARE__)) :\
 ((__HANDLE__)->CH4CVR = (__COMPARE__)))

extern k_msg_q_t msg_q;

u32 servo_pwm_ = 1500; // 舵机PWM

void reset_tentacle(void)
{
    servo_pwm_ = PWM_HL_NORMAL;
    __TIM_SET_COMPARE(TIM9, TIM_Channel_4, servo_pwm_);
}
// PC4/TIM9_CH4     ---舵机PWM, 无需映射
void tentable_init()
{
    u16 arr = 20000;
    u16 psc = 144 - 1;
    u16 ccp = 1500;
    GPIO_InitTypeDef GPIO_InitStructure={0};
    TIM_OCInitTypeDef TIM_OCInitStructure={0};
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure={0};

    // PC4/TIM9_CH4     ---舵机PWM, 无需映射
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_TIM9, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    TIM_TimeBaseInitStructure.TIM_Period = arr;
    TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM9, &TIM_TimeBaseInitStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = ccp;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC4Init(TIM9, &TIM_OCInitStructure);

    TIM_CtrlPWMOutputs(TIM9, ENABLE);
    TIM_OC1PreloadConfig( TIM9, TIM_OCPreload_Disable);
    TIM_ARRPreloadConfig( TIM9, ENABLE);
    TIM_Cmd(TIM9, ENABLE);
    TIM_CCxCmd(TIM9, TIM_Channel_4, TIM_CCx_Enable);
}

void tentable_push(en_push_method_t mtd)
{
    switch (mtd)
    {
    case PUSH_METHOD_ON:
        servo_pwm_ = PWM_HL_ON;
        break;
    case PUSH_METHOD_OFF:
        servo_pwm_ = PWM_HL_OFF;
        break;
    default:
        break;
    }
    __TIM_SET_COMPARE(TIM9, TIM_Channel_4, servo_pwm_);
    tos_task_delay(500);
    reset_tentacle();
}
