/*
 * tentacle.h
 *
 *  Created on: Jul 17, 2022
 *      Author: Administrator
 */

#ifndef USER_TENTACLE_H_
#define USER_TENTACLE_H_

typedef enum en_push_method
{
  PUSH_METHOD_ON,
  PUSH_METHOD_OFF
} en_push_method_t;

void tentable_init();
void tentable_push(en_push_method_t mtd);

#endif /* USER_TENTACLE_H_ */
